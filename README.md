# AppMonolitico Estudiantes

## Autor: Juan Carlos Zelaya Reyes
##### Inicio: Sabado 18-05-2021
##### Fecha de Entrega: xx de Mayo de 2021
##### Objetivo:  El trabajo consiste en implementar Integración continua, Entrega continua y Despliegue Continuo para la aplicación Monolitica.
 
 Monolítica Descripción
Utilizar una aplicación que disponga hecha en Maven que genere un artefacto de tipo WAR.
Crear un repositorio bitbucket público y alojar la aplicación.
Crear un pipeline(Jenkinsfile) con los siguientes stages: Build
Testing(Junit + Jacoco)
Sonar (cobertura mayor a 60%)
Artifactory Despliegue en Jboss.

## Comenzando 🚀

_Esta aplicación ha sido diseñada para optar al titulo de Arquitecto Devops._


### Consideraciones 📋

_Requisitos de configuración local

```
Prepare un entorno usando una instancia Ubuntu 20.04, puede utilizar cualquier proveedor de nube 
(AWS, Azure, Google Cloud, Digital Ocean, Linode), se recomienda estos 2 últimos dado que ofrecen un crédito
gratuito de $100. Recuerde eliminar el entorno para evitar cobros.
```

### Instalación 🔧

_Se ha usado Terraform para aprovisionar la creacion de la instancia en AWS_

_Clonar Proyecto_

```
git clone https://bitbucket.org/IngDevops/demojavamonolitico.git

```

## Autor ✒️

* **Juan Carlos Zelaya. Reyes** - *



## Licencia 📄

Este proyecto está bajo la Licencia GPL - mira el archivo [LICENSE.md](LICENSE.md) para detalles

## Expresiones de Gratitud 🎁

* Gracias por la oportunidad de realizar esta prueba tecnica 📢 
* 🤓.




---
⌨️   Juan Carlos Zelaya Reyes  🤓
